# README #

### What is this repository for? ###

This repository contains source code of Libertex's
Practical Assingment for Java Developers.

This application imports data from CSV file to Postgres database.
Data consists of Contacts and Subscriptions.

Applicaiton provides API for:
```
1.   Create contact
2.   Check subscription (Client_id and Subscriotion_type as parameters 
3.   Find By (any of fields from CSV should be used as parameter)
4.   Subscribe client to new subscription type
```

### Prerequisites ###

In order to successfully start this application,
Postgres server with empty database named **libertex** should be started.

### Configuration ###

Configuration file is located in **src/main/resources/application.properties** 
and all configurations provided bellow are related to that file.

Proper configuration file (in my local env) looks as follows:
```
## Base API path
server.servlet.context-path=/api/v1

## Spring DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
spring.datasource.url=jdbc:postgresql://localhost:5432/libertex
spring.datasource.username=postgres
spring.datasource.password=postgres

# Disable feature detection by this undocumented parameter. Check the org.hibernate.engine.jdbc.internal.JdbcServiceImpl.configure method for more details.
spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults = false

# Because detection is disabled you have to set correct dialect by hand.
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL9Dialect

# Hibernate ddl auto (create, create-drop, validate, update)
spring.jpa.hibernate.ddl-auto=validate

#Data import
data.import.execute=false

#Path to csv file
csv.file.path=/Users/obradsimic/data
csv.file.name=export.csv
```

### Technologies ###

Technologies that I used to implement this solution are:
```
1.   Postgres
2.   Spring Boot framework
```

### First time run ###

In order to sucessfully run this application for the first time,
we need to import some data to work with.

This is done by changing following properties:
```
# Hibernate ddl auto (create, create-drop, validate, update)
spring.jpa.hibernate.ddl-auto=create #in order to create db tables, Hibernate should be run in this mode

#Data import
data.import.execute=true #otherwise, import won't be run

#Path to csv file
csv.file.path=#path to the folder in which is your .csv (/Users/obradsimic/data on my MAC OS, for example)
csv.file.name=#name of the csv to import (export.csv)
```

### IMPORTANT ###

After successfull first time run,
please change following properties as listed below:
```
#Data import
data.import.execute=false

# Hibernate ddl auto (create, create-drop, validate, update)
spring.jpa.hibernate.ddl-auto=validate

```
This should be done in order to prevent Hibernate from re-creating tables and re-importing data.

### Testing ###

Testing should be done in Postman, 
and for that purpose **libertex.postman_collection.json** is provided.