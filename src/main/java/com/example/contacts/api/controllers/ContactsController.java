package com.example.contacts.api.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.contacts.api.dto.ContactDto;
import com.example.contacts.api.dto.SubscriptionDto;
import com.example.contacts.api.helpers.assemblers.ContactAssembler;
import com.example.contacts.api.helpers.assemblers.SubscriptionAssembler;
import com.example.contacts.api.models.Subscription;
import com.example.contacts.api.services.ContactsService;

@RestController
public class ContactsController {
	
	private ContactsService contactsService;
	
	@Autowired
	public ContactsController(ContactsService contactsService) {
		this.contactsService = contactsService;
	}
	
	@GetMapping(path="/contacts")
	public ResponseEntity<List<ContactDto>> findByCriteria(@Valid @RequestBody ContactDto dto, Pageable page) {
		final List<ContactDto> result = ContactAssembler.toDtoList(contactsService.findByCriteria(dto, page).getContent());
		
		if (!CollectionUtils.isEmpty(result)) {
			return ResponseEntity.ok(result);
		}
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(ContactAssembler.emptyDtoList());
	}
	
	@GetMapping(path="/check_subscription")
	public ResponseEntity<SubscriptionDto> findByCriteria(@RequestParam("clientId") String clientId, @RequestParam("subscriptionType") String subscriptionType) {
		final List<Subscription> result = contactsService.checkSubscriptions(clientId, subscriptionType);
		
		if (!CollectionUtils.isEmpty(result)) {
			return ResponseEntity.ok(SubscriptionAssembler.toDto(result.get(0)));
		}
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(SubscriptionAssembler.emptyDto());
	}
}
