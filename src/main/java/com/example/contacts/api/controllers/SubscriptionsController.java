package com.example.contacts.api.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.contacts.api.dto.SubscriptionDto;
import com.example.contacts.api.helpers.assemblers.SubscriptionAssembler;
import com.example.contacts.api.models.Subscription;
import com.example.contacts.api.services.SubscriptionService;

@RestController
public class SubscriptionsController {

	private SubscriptionService subscriptionService;
	
	@Autowired
	public SubscriptionsController(SubscriptionService subscriptionService) {
		this.subscriptionService = subscriptionService;
	}
	
	@GetMapping(path="/subscriptions")
	public ResponseEntity<List<SubscriptionDto>> findByCriteria(@RequestBody SubscriptionDto dto, Pageable page) {
		final List<Subscription> result = subscriptionService.findByCriteria(dto, page).getContent();
		
		if (!CollectionUtils.isEmpty(result)) {
			return ResponseEntity.ok(SubscriptionAssembler.toDtoList(result));
		}
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(SubscriptionAssembler.emptyDtoList());
	}
	
	@GetMapping(path="/subscriptions/{id}")
	public ResponseEntity<SubscriptionDto> findById(@PathVariable("id") long subscriptionId) {
		SubscriptionDto result = subscriptionService.findById(subscriptionId);
		
		if (!SubscriptionAssembler.isEmptyDto(result)) {
			return ResponseEntity.ok(result);
		}
		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(SubscriptionAssembler.emptyDto());
	}
	
	@PostMapping("/subscriptions")
	public ResponseEntity<SubscriptionDto> newSubscription(@Valid @RequestBody SubscriptionDto dto) throws URISyntaxException {
		final Subscription result = subscriptionService.save(dto);
		
		if (!SubscriptionAssembler.isEmptyModel(result)) {
			return ResponseEntity.created(new URI("/subscriptions/" + result.getSubscriptionId())).body(dto);
		}
		
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(dto);
	}
}
