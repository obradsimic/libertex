package com.example.contacts.api.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class ContactDto implements Serializable {
	
	private static final long serialVersionUID = -291502366256902468L;
	
	@NotNull 
	private String clientId;
	
	@NotNull 
	private String broker;
	
	@NotNull 
	private String country;
	
	@NotNull 
	private String language;
	
	@NotNull 
	private String identifier;
	
	public ContactDto(String clientId, String broker, String country, String language, String identifier) {
		this.clientId = clientId;
		this.broker = broker;
		this.country = country;
		this.language = language;
		this.identifier = identifier;
	}

	public ContactDto(String clientId) {
		this.clientId = clientId;
	}

	public String getClientId() {
		return clientId;
	}

	public String getBroker() {
		return broker;
	}

	public String getCountry() {
		return country;
	}

	public String getLanguage() {
		return language;
	}

	public String getIdentifier() {
		return identifier;
	}
	
	
}
