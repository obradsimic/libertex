package com.example.contacts.api.dto;

public class ContactSubscriptionDto {

	private String clientId;
	private String broker;
	private String country;
	private String language;
	private String identifier;
	private String subsritionType;
	private String accountCode;

	public ContactSubscriptionDto() {
		
	}

	public ContactSubscriptionDto(String clientId, String broker, String country, String language, String identifier, String subsritionType, String accountCode) {
		this.clientId = clientId;
		this.broker = broker;
		this.country = country;
		this.language = language;
		this.identifier = identifier;
		this.subsritionType = subsritionType;
		this.accountCode = accountCode;
	}

	public String getSubsritionType() {
		return subsritionType;
	}

	public void setSubsritionType(String subsritionType) {
		this.subsritionType = subsritionType;
	}

	public String getClientId() {
		return clientId;
	}
	
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	public String getBroker() {
		return broker;
	}
	
	public void setBroker(String broker) {
		this.broker = broker;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public String getAccountCode() {
		return accountCode;
	}
	
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
}
