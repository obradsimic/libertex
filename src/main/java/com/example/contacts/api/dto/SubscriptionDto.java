package com.example.contacts.api.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class SubscriptionDto {
	
	@NotNull
	@NotEmpty
	private String subsritionType;
	
	@NotNull
	@NotEmpty
	private String accountCode;
	
	@NotNull
	@NotEmpty
	private String clientId;
	
	public SubscriptionDto(String subsritionType, String accountCode, String clientId) {
		this.subsritionType = subsritionType;
		this.accountCode = accountCode;
		this.clientId = clientId;
	}

	public String getSubsritionType() {
		return subsritionType;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public String getClientId() {
		return clientId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountCode == null) ? 0 : accountCode.hashCode());
		result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
		result = prime * result + ((subsritionType == null) ? 0 : subsritionType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscriptionDto other = (SubscriptionDto) obj;
		if (accountCode == null) {
			if (other.accountCode != null)
				return false;
		} else if (!accountCode.equals(other.accountCode))
			return false;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (subsritionType == null) {
			if (other.subsritionType != null)
				return false;
		} else if (!subsritionType.equals(other.subsritionType))
			return false;
		return true;
	}
}
