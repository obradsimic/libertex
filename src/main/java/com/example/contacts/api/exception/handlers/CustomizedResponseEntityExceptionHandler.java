package com.example.contacts.api.exception.handlers;

import java.util.LinkedList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.contacts.api.exception.errors.ErrorDetails;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Override
	  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	    
		List<ErrorDetails> errors = new LinkedList<ErrorDetails>();
		
		for (ObjectError error: ex.getBindingResult().getAllErrors()) {
			FieldError fieldError = (FieldError) error;
			ErrorDetails errorDetails = new ErrorDetails(fieldError.getField(), fieldError.getDefaultMessage());
			
			errors.add(errorDetails);
		}
		
	    return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
	  }

}
