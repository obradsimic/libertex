package com.example.contacts.api.helpers;

import java.io.File;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class CsvDataLoader {
	private static Logger LOGGER = LogManager.getLogger(CsvDataLoader.class);
	
	@SuppressWarnings("deprecation")
	public static <T> List<T> loadObjectList(Class<T> type, String filePath, String fileName) {
	    try {
	    	LOGGER.info("Started loading data .....");
	    	
	        CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
	        CsvMapper mapper = new CsvMapper();
	        File file = new File(filePath + "/" + fileName);
	        MappingIterator<T> readValues = mapper.reader(type).with(bootstrapSchema).readValues(file);
	        
	        LOGGER.info("Done loading data!");
	        return readValues.readAll();
	    } catch (Exception e) {
	    	LOGGER.error(e);
	        return Collections.emptyList();
	    }
	}

}
