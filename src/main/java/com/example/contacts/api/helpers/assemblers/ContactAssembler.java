package com.example.contacts.api.helpers.assemblers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.example.contacts.api.dto.ContactDto;
import com.example.contacts.api.dto.SubscriptionDto;
import com.example.contacts.api.models.Contact;

public class ContactAssembler {
	
	private static final List<ContactDto> EMPTY_DTO_LIST = new LinkedList<ContactDto>();

	public static ContactDto toDto(Contact contact) {
		return new ContactDto(
			contact.getClientId(), 
			contact.getBroker(), 
			contact.getCountry(), 
			contact.getLanguage(), 
			contact.getIdentifier()
		);
	}

	public static List<ContactDto> toDtoList(List<Contact> content) {
		List<ContactDto> result = new ArrayList<ContactDto>(content.size());
		
		for (Contact contact: content) {
			result.add(ContactAssembler.toDto(contact));
		}
		
		return result;
	}
	
	public static ContactDto toModel(SubscriptionDto dto) {
		return new ContactDto(dto.getClientId());
	}

	public static List<ContactDto> emptyDtoList() {
		return EMPTY_DTO_LIST;
	}
}
