package com.example.contacts.api.helpers.assemblers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.example.contacts.api.dto.SubscriptionDto;
import com.example.contacts.api.models.Subscription;

public class SubscriptionAssembler {
	
	private static final SubscriptionDto EMPTY_DTO = new SubscriptionDto("", "", "");
	private static final List<SubscriptionDto> EMPTY_DTO_LIST = new LinkedList<SubscriptionDto>();
	private static final Subscription EMPTY_MODEL = new Subscription();
	
	public static SubscriptionDto toDto(Subscription subscription) {
		return new SubscriptionDto(subscription.getSubscriptionType(), subscription.getAccountCode(), subscription.getContact().getClientId());
	}

	public static List<SubscriptionDto> toDtoList(List<Subscription> content) {
		List<SubscriptionDto> result = new ArrayList<SubscriptionDto>(content.size());
		
		for (Subscription subscription: content) {
			result.add(SubscriptionAssembler.toDto(subscription));
		}
		
		return result;
	}
	
	public static Subscription toModel(SubscriptionDto dto) {
		return new Subscription(dto.getSubsritionType(), dto.getAccountCode(), null);
	}

	public static SubscriptionDto emptyDto() {
		return EMPTY_DTO;
	}

	public static List<SubscriptionDto> emptyDtoList() {
		return EMPTY_DTO_LIST;
	}

	public static boolean isEmptyDto(SubscriptionDto dto) {
		return dto.equals(EMPTY_DTO);
	}

	public static Subscription emptyModel() {
		return EMPTY_MODEL;
	}
	
	public static boolean isEmptyModel(Subscription model) {
		return model.equals(EMPTY_MODEL);
	}
}
