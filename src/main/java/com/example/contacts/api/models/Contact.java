package com.example.contacts.api.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="contacts", indexes = { @Index(name = "IDX_CLIENTID_LANGUAGE_IDENTIFIER", columnList = "clientId,language,identifier") })
public class Contact {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long contactId;
	
	@Column(name="clientId")
	private String clientId;
	
	@Column(name="broker")
	private String broker;
	
	@Column(name="country")
	private String country;
	
	@Column(name="language")
	private String language;
	
	@Column(name="identifier")
	private String identifier;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contact")
	private List<Subscription> subscriptions;

	public Contact(String clientId, String broker, String country, String language, String identifier) {
		this.clientId = clientId;
		this.broker = broker;
		this.country = country;
		this.language = language;
		this.identifier = identifier;
	}

	public Contact() {

	}

	public long getContactId() {
		return contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}
}
