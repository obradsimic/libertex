package com.example.contacts.api.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.contacts.api.models.Contact;

public interface ContactsRepository extends PagingAndSortingRepository<Contact,Long>, JpaSpecificationExecutor<Contact> {

	@Query("select c from Contact c where c.clientId = :clientId and c.language = :language and c.identifier = :identifier")
	public Contact findByUniqueIndex(@Param("clientId") String clientId, @Param("language") String language, @Param("identifier") String identifier);
}
