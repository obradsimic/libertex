package com.example.contacts.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.contacts.api.models.Subscription;

public interface SubscriptionsRepository extends JpaRepository<Subscription,Long>, JpaSpecificationExecutor<Subscription>  {
	
}
