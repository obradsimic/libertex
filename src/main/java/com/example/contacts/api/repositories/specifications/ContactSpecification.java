package com.example.contacts.api.repositories.specifications;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.example.contacts.api.dto.ContactDto;
import com.example.contacts.api.models.Contact;

public class ContactSpecification implements Specification<Contact> {
	
	private static final long serialVersionUID = -2810974089532994267L;
	
	private ContactDto criteria;
	
	public ContactSpecification(ContactDto criteria) {
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(Root<Contact> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		final List<Predicate> predicates = new LinkedList<Predicate>();
		
		if (criteria.getBroker() != null && !StringUtils.isEmpty(criteria.getBroker())) {
			predicates.add(criteriaBuilder.equal(root.get("broker"), criteria.getBroker()));
		}
		
		if (criteria.getClientId() != null && !StringUtils.isEmpty(criteria.getClientId())) {
			predicates.add(criteriaBuilder.equal(root.get("clientId"), criteria.getClientId()));
		}
		
		if (criteria.getCountry() != null && !StringUtils.isEmpty(criteria.getCountry())) {
			predicates.add(criteriaBuilder.equal(root.get("country"), criteria.getCountry()));
		}
		
		if (criteria.getIdentifier() != null && !StringUtils.isEmpty(criteria.getIdentifier())) {
			predicates.add(criteriaBuilder.equal(root.get("identifier"), criteria.getIdentifier()));
		}
		
		if (criteria.getLanguage() != null && !StringUtils.isEmpty(criteria.getLanguage())) {
			predicates.add(criteriaBuilder.equal(root.get("language"), criteria.getLanguage()));
		}
		
		return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
	}

}
