package com.example.contacts.api.repositories.specifications;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.example.contacts.api.dto.SubscriptionDto;
import com.example.contacts.api.models.Subscription;

public class SubscriptionSpecification implements Specification<Subscription> {

	private static final long serialVersionUID = -2091424146458554991L;
	
	private SubscriptionDto criteria;
	
	public SubscriptionSpecification(SubscriptionDto criteria) {
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(Root<Subscription> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		final List<Predicate> predicates = new LinkedList<Predicate>();
		
		if (criteria.getAccountCode() != null && !StringUtils.isEmpty(criteria.getAccountCode())) {
			predicates.add(criteriaBuilder.equal(root.get("accountCode"), criteria.getAccountCode() ));
		}
		
		if (criteria.getSubsritionType() != null && !StringUtils.isEmpty(criteria.getSubsritionType())) {
			predicates.add(criteriaBuilder.equal(root.get("subscriptionType"), criteria.getSubsritionType()));
		}
		
		return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
	}

}
