package com.example.contacts.api.services;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.example.contacts.api.dto.ContactDto;
import com.example.contacts.api.models.Contact;
import com.example.contacts.api.models.Subscription;
import com.example.contacts.api.repositories.ContactsRepository;
import com.example.contacts.api.repositories.specifications.ContactSpecification;

@Service
public class ContactsService {

	private ContactsRepository contactsRepo;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	public ContactsService(ContactsRepository contactsRepo) {
		this.contactsRepo = contactsRepo;
	}
	
	@Transactional
	public boolean create(ContactDto dto) {
		Contact contact = contactsRepo.findByUniqueIndex(dto.getClientId(), dto.getLanguage(), dto.getIdentifier());
		
		if (contact != null) {
			return false;
		}
		
		contact = new Contact(dto.getClientId(), dto.getBroker(), dto.getCountry(), dto.getLanguage(), dto.getIdentifier());
		contactsRepo.save(contact);
		
		return true;
	}
	
	public Page<Contact> findByCriteria(ContactDto criteria, Pageable pageable) {
		return contactsRepo.findAll(new ContactSpecification(criteria), pageable);
	}
	
	public List<Subscription> checkSubscriptions(String clientId, String subscriptionType) {
		ContactSpecification criteria = new ContactSpecification(new ContactDto(clientId, null, null, null, null));
		Optional<Contact> contact = contactsRepo.findOne(criteria);
		
		if (contact.isPresent()) {
			List<Subscription> result = contact.get().getSubscriptions()
				.stream()
				.filter( x -> x.getSubscriptionType().equals(subscriptionType) )
				.collect(Collectors.toList()); 
				
			return CollectionUtils.isEmpty(result) ? Collections.emptyList() : result;
		}
		
		return Collections.emptyList();
	}
}
