package com.example.contacts.api.services;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.contacts.api.dto.ContactSubscriptionDto;
import com.example.contacts.api.helpers.CsvDataLoader;
import com.example.contacts.api.models.Contact;
import com.example.contacts.api.models.Subscription;
import com.example.contacts.api.repositories.ContactsRepository;
import com.example.contacts.api.repositories.SubscriptionsRepository;

@Service
public class DataImporterService {
	private static Logger LOGGER = LogManager.getLogger(DataImporterService.class);
	
	private ContactsRepository contactsRepo;
	private SubscriptionsRepository subscriptionsRepo;
	
	private String pathToCsvFile;
	private String csvFileName;
	private boolean performDataImport;

	@Autowired
	public DataImporterService(ContactsRepository contactsRepo, SubscriptionsRepository subscriptionsRepo, @Value("${csv.file.path}") String pathToCsvFile, @Value("${csv.file.name}") String csvFileName, @Value("${data.import.execute}") boolean performDataImport) {
		this.contactsRepo = contactsRepo;
		this.subscriptionsRepo = subscriptionsRepo;
		this.pathToCsvFile = pathToCsvFile;
		this.csvFileName = csvFileName;
		this.performDataImport = performDataImport;
	}
	
	@PostConstruct
	public void init() {
		importDataFromCsv(performDataImport);
	}
	
	private void importDataFromCsv(boolean performDataImport) {

		if (!performDataImport) return;
		
		LOGGER.info("Importing data from csv .....");
		LOGGER.info("Path to csv file: " + pathToCsvFile + "/" + csvFileName);
		
		final List<ContactSubscriptionDto> data = CsvDataLoader.loadObjectList(ContactSubscriptionDto.class, pathToCsvFile, csvFileName);
		
		for (ContactSubscriptionDto dto: data) {
			importSubscription(dto, findOrImportContact(dto));
		}
		
		LOGGER.info("Done importing data from csv!");
	}

	@Transactional
	private Contact findOrImportContact(ContactSubscriptionDto dto) {
		LOGGER.info("Finding or importing contact .....");
		
		Contact contact = contactsRepo.findByUniqueIndex(dto.getClientId(), dto.getLanguage(), dto.getIdentifier());
		
		if (contact == null) {
			contact = new Contact(dto.getClientId(), dto.getBroker(), dto.getCountry(), dto.getLanguage(), dto.getIdentifier());
			contact = contactsRepo.save(contact);
		}
		
		LOGGER.info("Returned contact with id:" + contact.getContactId());
		return contact;
	}
	
	@Transactional
	private void importSubscription(ContactSubscriptionDto dto, Contact contact) {
		LOGGER.info("Importing subscription .....");
		Subscription subscription = new Subscription(dto.getSubsritionType(), dto.getAccountCode(), contact);
		subscriptionsRepo.save(subscription);
		LOGGER.info("Imported subscription with id: " + subscription.getSubscriptionId());
	}
}
