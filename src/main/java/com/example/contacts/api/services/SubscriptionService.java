package com.example.contacts.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.contacts.api.dto.SubscriptionDto;
import com.example.contacts.api.helpers.assemblers.ContactAssembler;
import com.example.contacts.api.helpers.assemblers.SubscriptionAssembler;
import com.example.contacts.api.models.Contact;
import com.example.contacts.api.models.Subscription;
import com.example.contacts.api.repositories.ContactsRepository;
import com.example.contacts.api.repositories.SubscriptionsRepository;
import com.example.contacts.api.repositories.specifications.ContactSpecification;
import com.example.contacts.api.repositories.specifications.SubscriptionSpecification;

@Service
public class SubscriptionService {
	
	private SubscriptionsRepository subscriptionsRepository;
	private ContactsRepository contactsRepository;

	@Autowired
	public SubscriptionService(SubscriptionsRepository subscriptionRepo, ContactsRepository contactRepo) {
		this.subscriptionsRepository = subscriptionRepo;
		this.contactsRepository = contactRepo;
	}
	
	public Page<Subscription> findByCriteria(SubscriptionDto criteria, Pageable pageable) {
		return subscriptionsRepository.findAll(new SubscriptionSpecification(criteria), pageable);
	}

	public Subscription save(SubscriptionDto dto) {
		
		final Optional<Contact> contact = contactsRepository.findOne(new ContactSpecification(ContactAssembler.toModel(dto)));
		
		if (contact.isPresent() && !isAlreadySubscribed(contact.get().getSubscriptions(), dto)) {
			final Subscription subscription = SubscriptionAssembler.toModel(dto);
			subscription.setContact(contact.get());
			
			return subscriptionsRepository.save(subscription);
		}
		
		return SubscriptionAssembler.emptyModel();
	}

	private boolean isAlreadySubscribed(List<Subscription> subscriptions, SubscriptionDto dto) {
		
		return subscriptions
			.stream()
			.anyMatch(x -> x.getSubscriptionType().equals(dto.getSubsritionType()) && x.getAccountCode().equals(dto.getAccountCode()));
	}

	public SubscriptionDto findById(long subscriptionId) {
		
		final Optional<Subscription> result = subscriptionsRepository.findById(subscriptionId);
		
		if (result.isPresent()) {
			return SubscriptionAssembler.toDto(result.get());
		}
		
		return SubscriptionAssembler.emptyDto();
	}
}
